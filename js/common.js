
$(document).ready(function () {
    $("#form1").submit(function () {
        $.ajax({
            type: "POST"
            , url: "mail.php"
            , data: $(this).serialize()
        }).done(function () {
            $(this).find("input").val("");
            $("#form1").trigger("reset");
            $.fancybox.open($("#ok"));
            setTimeout(function () {
                $.fancybox.close();
            }, 1500);
        });
        return false;
    });
});
$(".gambur-big").click(function () {
  
   $("header.top .container").animate({"height": "+135px"}, "slow");
     
    $(".tel").css('margin-bottom', '15px');
    $("nav.menu-top-block").css('display', 'block');
    $(".gambur-big").css('display', 'none');
     $(".klick").css('display', 'block');
     $("a.call-back").css('display', 'block');
  
});

jQuery(function ($) {
    $(".telehone").mask("+38 (999) 999-99-99");
});
$('.autoplay').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
       responsive: [
       {
      breakpoint: 1122,
      settings: {
        slidesToShow: 3,
    
      }
    },


    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
    
      }
    },


             {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
    
      }
    },


             {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
    
      }
    },

             {
      breakpoint: 452,
      settings: {
        slidesToShow: 1,
    
      }
    }
           
  ] 

});
$('.slide-rew').slick({
    centerPadding: '20px',
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000,
    speed: 2000,
   responsive: [
    {
      breakpoint: 1201,
      settings: {
        slidesToShow: 3,
    
      }
    },
         {
      breakpoint: 993,
      settings: {
        slidesToShow: 2,
    
      }
    },
         {
      breakpoint: 769,
      settings: {
        slidesToShow: 1,
    
      }
    }
  ] 
});


  $("a.call-back").fancybox({
    fitToView	: true,

    });
 $("a.zakaz-btn").fancybox({
    fitToView	: false,

    });
$("a.kons-btn").fancybox({
    fitToView	: false,

    });
$("a.zakaz-sc10").fancybox({
    fitToView	: false,

    });
$("a.pop-rew").fancybox({
    fitToView	: false,

    });
$("a.zakaz-req").fancybox({
   
 fitToView	: false,
    });

$(".gambur").click(function () {
      $("nav.menu-top ul").slideToggle("slow");
});
$(".head-prof-teem").click(function () {
      $(".prof-teem-box.new").slideToggle("slow");
});
$(".map-box").click(function () {
      $(".map-box").css("display", "none");
});

  window.onload = function () {
      if (screen.width <= 480) {
          var mvp = document.getElementById('myViewport');
          mvp.setAttribute('content', 'width=480');
      }
  };
